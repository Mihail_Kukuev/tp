package Classes;

import Classes.Boundary.LoginForm;
import Classes.Controllers.LoginController;

public class LoginAction extends WindowAction {

    public LoginAction(GUI frame) {
        super(frame);
    }

    @Override
    public void performAction() {
        LoginController controller =
                new LoginController(frame.getDataAccessor(), frame.getSession());
        LoginForm form = new LoginForm(controller);

        frame.setContentPane(form);
        frame.revalidate();
        frame.repaint();

        controller.startAction();
    }
}
