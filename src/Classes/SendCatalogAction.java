package Classes;

import Classes.Boundary.ClientsChooser;
import Classes.Controllers.ClientsController;


public class SendCatalogAction extends WindowAction {

    public SendCatalogAction(GUI frame) {
        super(frame);
    }

    @Override
    public void performAction() {


        ClientsController controller = new ClientsController(frame.getDataAccessor());
        ClientsChooser clientsChooser = new ClientsChooser(controller);

        frame.setContentPane(clientsChooser);
        frame.revalidate();
        frame.repaint();

        controller.startAction();
    }
}
