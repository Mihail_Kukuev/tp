package Classes;


import Classes.Boundary.DataAccessor;

import javax.swing.*;

public abstract class WindowAction {

    protected GUI frame;

    public WindowAction(GUI frame) {
        this.frame = frame;
    }

    public abstract void performAction();
}
