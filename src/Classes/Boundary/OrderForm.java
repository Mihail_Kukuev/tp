package Classes.Boundary;

import Classes.ClientManagerAction;
import Classes.Controllers.OrderController;
import Classes.Entities.Product;
import Classes.Entities.ProductsList;
import Classes.GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class OrderForm extends JPanel {

    private static final String[] COLUMN_NAMES_SHOW = {"product name", "is selected"};
    private static final String[] COLUMN_NAMES_COST = {"product name", "product cost", "product count"};

    private DefaultTableModel productTableModel;
    private DefaultTableModel selectedTableModel;
    private OrderController controller;
    private JTextField addressField;
    private JTextField phoneField;

    private ProductsList all;
    private ProductsList selected;

    public OrderForm(OrderController controller) {
        this.controller = controller;
        controller.setOrderForm(this);

        setSize(800, 600);
        setVisible(false);
    }


    public void showCost(int totalCost) {
        clearView();

        setLayout(new GridLayout(6, 1));
        add(new JLabel("Total cost: " + totalCost));

        add(new JLabel("address"));
        addressField = new JTextField(100);
        add(addressField);
        add(new JLabel("phone"));
        phoneField = new JTextField(100);
        add(phoneField);

        JButton saveOrder = new JButton("save order");
        add(saveOrder);
        saveOrder.addActionListener(e -> saveOrder());

        JButton backButton = new JButton("back");
        backButton.addActionListener(e -> showCostFields(selected));
        add(backButton);
    }

    private void saveOrder() {
        Map<String, String> formData = new HashMap<>();

        formData.put("address", addressField.getText());
        formData.put("phone", phoneField.getText());

        controller.saveOrder(formData);
    }

    private void selectProducts() {
        ArrayList<Integer> indices = new ArrayList<>();

        for (int i = 0; i < productTableModel.getRowCount(); i++) {
            if ((boolean) productTableModel.getValueAt(i, 1)) {
                indices.add(i);
            }
        }

        controller.selectProducts(indices);
    }

    private void clearView() {
        removeAll();
        revalidate();
        repaint();
        setVisible(true);
    }

    public void showCostFields(ProductsList selectProductsList) {
        this.selected = selectProductsList;

        clearView();

        selectedTableModel = new DefaultTableModel(COLUMN_NAMES_COST, 0) {
            @Override
            public Class getColumnClass(int column) {
                if (column == 2)
                    return Integer.class;
                return super.getColumnClass(column);
            }
        };
        JTable selectedTable = new JTable(selectedTableModel);

        for (Product selectedProduct : selectProductsList) {
            String name = selectedProduct.getName();
            String cost = "$" + selectedProduct.getCost();
            int count = selectedProduct.getAmount();

            selectedTableModel.addRow(new Object[]{name, cost, count});
        }

        setLayout(new BorderLayout());

        add(selectedTable);


        JButton submitOrder = new JButton("submit order");
        add(submitOrder, BorderLayout.SOUTH);
        submitOrder.addActionListener((e) -> submitOrder());

        JButton backButton = new JButton("back");
        backButton.addActionListener(e -> showOrderForm(all));
        add(backButton, BorderLayout.NORTH);
    }


    public void showOrderForm(ProductsList products) {
        this.all = products;

        clearView();

        productTableModel = new DefaultTableModel(COLUMN_NAMES_SHOW, 0) {
            @Override
            public Class getColumnClass(int column) {
                if (column == 1)
                    return Boolean.class;
                return super.getColumnClass(column);
            }
        };
        JTable productTable = new JTable(productTableModel);
        setLayout(new BorderLayout());

        add(productTable, BorderLayout.CENTER);

        for (Product product : products) {
            productTableModel.addRow(new Object[]{product.getName(), false});
        }

        JButton selectProducts = new JButton("select products");
        add(selectProducts, BorderLayout.SOUTH);
        selectProducts.addActionListener(e -> this.selectProducts());

        JButton backButton = new JButton("back");
        GUI frame = (GUI)SwingUtilities.getWindowAncestor(this);
        backButton.addActionListener(e -> frame.performAction(new ClientManagerAction(frame)));
        add(backButton, BorderLayout.NORTH);
    }

    public void showResult(boolean isSuccessful) {
        GUI frame = (GUI) SwingUtilities.getWindowAncestor(this);

        if (isSuccessful) {
            JOptionPane.showMessageDialog(frame,
                    "Order Created", "success",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(frame,
                    "Order was not created because if the error occurred",
                    "error", JOptionPane.ERROR_MESSAGE);
        }

        frame.performAction(new ClientManagerAction(frame));
    }

    private void submitOrder() {
        ArrayList<Integer> counts = new ArrayList<>();

        for (int i = 0; i < selectedTableModel.getRowCount(); ++i) {
            counts.add((Integer) selectedTableModel.getValueAt(i, 2));
            System.out.println(selectedTableModel.getValueAt(i, 2));
        }

        controller.submitOrder(counts);
    }
}