package Classes.Boundary;

import Classes.Entities.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ������
 * @version 1.0
 * @created 16-May-2016 10:25:28 PM
 */
public class DataAccessor {

//	public TransportList m_TransportList;
//	public ManufacturersList m_ManufacturersList;

	private Map<String, Map<String, String>> stubUserDb;
	private ArrayList<Product> stubProductDb;

	public DataAccessor(){
		stubUserDb = new HashMap<>();
		Map<String, String> test_data = new HashMap<>();
		test_data.put("password", "test");
		test_data.put("role", "client manager");
		stubUserDb.put("test", test_data);


		stubProductDb = new ArrayList<>();
		stubProductDb.add(new Product(23, 600, "ipad air 2"));
		stubProductDb.add(new Product(100, 200, "ipod nano"));
	}

	public void finalize() throws Throwable {

	}
	public void addManufacturer(){

	}

//	/**
//	 *
//	 * @param order
//	 */
	public boolean addOrder(Order order){

		//blabla
		System.out.println("saved order: ");
		System.out.println("address :" + order.getAddress());
		System.out.println("phone :" + order.getPhone());
		ProductsList products = order.getProductsList();
		for (Product product : products) {
			System.out.println(product.getName() + "(" + product.getCost() + ") :" + product.getAmount());
		}
		System.out.println("total: " + order.getCost());

		return false;
	}

	public void addTransportCompany(){

	}

	/**
	 * 
	 * @param user
	 */
	public void authenticate(User user){


		System.out.println(user.getName() + " " + user.getPassword() + "trying");
		Map<String, String> userData = stubUserDb.get(user.getName());
		if (userData == null) {
			user.setRole("nil");
			return;
		}

		if (user.getPassword().equals(userData.get("password"))) {
			user.setRole(userData.get("role"));
		} else {
			user.setRole("nil");
		}
	}

//	public void deleteManufacturer(){
//
//	}
//
//	public void deleteTransportCompany(){
//
//	}
//
//	public void editManufacturer(){
//
//	}
//
//	public void editTransportCompany(){
//
//	}

	public ClientsList getClients(){
		ClientsList clients = new ClientsList();
		clients.add(new Client("misha_loh@mail.ru", "misha"));
		clients.add(new Client("malenkaya_piska@gmail.com", "vlad"));

		return clients;
	}

//	public List<Manufacturers> getManufactureres(){
//		return null;
//	}

	public ProductsList getProducts() {
		return new ProductsList(stubProductDb);
	}
}//end DataAccessor