package Classes.Boundary;

import Classes.ClientManagerAction;
import Classes.Entities.Client;
import Classes.Entities.ClientsList;
import Classes.Controllers.ClientsController;
import Classes.GUI;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

public class ClientsChooser extends JPanel {
	private static final String[] COLUMNS = {"email", "name", "selected"};

	private DefaultTableModel clientsModel;
	private ClientsController controller;

	public ClientsChooser(ClientsController controller) {
		setVisible(false);
		setSize(800, 600);

		this.controller = controller;
		controller.setClientsChooser(this);

	}

	public void showClients(ClientsList clients) {
		setVisible(true);
		clientsModel = new DefaultTableModel(COLUMNS, 0) {
			@Override
			public Class<?> getColumnClass(int i) {
				if (i == 2)
					return Boolean.class;
				else
					return super.getColumnClass(i);
			}
		};

		JTable clientsTable = new JTable(clientsModel);

		setLayout(new BorderLayout());
		add(clientsTable);

		for (Client client : clients) {
			clientsModel.addRow(new Object[]{client.getEmail(), client.getName(), false});
		}

		System.out.println(clientsTable.getRowCount());

		JButton confirmSendingButton = new JButton("confirm sending");
		confirmSendingButton.addActionListener(e -> confirmSending());
		add(confirmSendingButton, BorderLayout.SOUTH);

		JButton backButton = new JButton("back");
		GUI frame = (GUI)SwingUtilities.getWindowAncestor(this);
		backButton.addActionListener(e -> frame.performAction(new ClientManagerAction(frame)));
		add(backButton, BorderLayout.NORTH);
	}

	private void confirmSending() {
		ArrayList<Integer> indices = new ArrayList<>();
		for (int i = 0; i < clientsModel.getRowCount(); ++i)
			if ((Boolean)clientsModel.getValueAt(i, 2))
				indices.add(i);

		controller.sendCatalog(indices);
	}

	public void showSendingResult(boolean isSuccesful) {
		GUI frame = (GUI)SwingUtilities.getWindowAncestor(this);

		if (isSuccesful)
			JOptionPane.showMessageDialog(frame,
					"Catalog sent to selected clients",
					"success", JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(frame,
					"Catalog not send because of the error occurred",
					"error", JOptionPane.ERROR_MESSAGE);

		frame.performAction(new ClientManagerAction(frame));
	}
}