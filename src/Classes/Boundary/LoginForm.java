package Classes.Boundary;

import Classes.Controllers.LoginController;
import Classes.GUI;
import Classes.NilAction;
import Classes.WindowAction;
import util.Helper;

import javax.swing.*;


public class LoginForm extends JPanel {

	public LoginForm(LoginController controller){
		setVisible(false);
		setSize(800, 600);

		controller.setLoginForm(this);

		JLabel loginLabel = new JLabel("login");
		JTextField loginField = new JTextField(45);
		JLabel passwordLabel = new JLabel("password");
		JPasswordField passwordField = new JPasswordField(45);

		JButton loginButton = new JButton("login!");
		loginButton.addActionListener(e -> controller.doLogin(
				loginField.getText(),
				new String(passwordField.getPassword()))
		);

		add(loginLabel);
		add(loginField);
		add(passwordLabel);
		add(passwordField);
		add(loginButton);
	}

	public void showPage(String role){
		GUI frame = (GUI)SwingUtilities.getWindowAncestor(this);

		WindowAction actionToShow;
		try {
			actionToShow = Helper.roleToActionClass(role)
					.getConstructor(GUI.class).newInstance(frame);
		} catch (Exception e) {
			actionToShow = new NilAction(frame);
			e.printStackTrace();
		}

		System.out.println(actionToShow.getClass().getName());
		final WindowAction stub = actionToShow;
		new Thread(() -> frame.performAction(stub)).start();
	}
}