package Classes.Entities;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author ������
 * @version 1.0
 */
public class ProductsList extends ArrayList<Product> implements Serializable{

	public ProductsList(){
		super();
	}

	public ProductsList(ArrayList<Product> products) {
		super(products);
	}
}//end ProductsList