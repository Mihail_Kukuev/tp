package Classes.Entities;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ������
 * @version 1.0
 * @created 16-May-2016 10:25:29 PM
 */
public class Order implements Serializable{

	private int cost;
	private ProductsList productsList;
	private String address;
	private String phone;

	public Order(){

	}

	public Order(Map<String, String> formData, ProductsList productsList){
		address = formData.get("address");
		phone = formData.get("phone");

		this.productsList = productsList;
		this.cost = 0;
		for (Product product : productsList) {
			this.cost += product.getCost() * product.getAmount();
		}
	}

	public int getCost() {
		return cost;
	}

	public ProductsList getProductsList() {
		return productsList;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}
}//end Order