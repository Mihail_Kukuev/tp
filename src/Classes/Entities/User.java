package Classes.Entities;


/**
 * @author ������
 * @version 1.0
 */
public class User {

	private String name;
	private String password;
	private String role;

	public User(String name, String password){
		this.name = name;
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}
}//end User