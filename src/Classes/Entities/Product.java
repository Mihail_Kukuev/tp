package Classes.Entities;


import java.io.Serializable;

/**
 * @author ������
 * @version 1.0
 * @created 16-May-2016 10:25:29 PM
 */
public class Product implements Serializable{

	private int amount;
	private int cost;
	private String name;

	public Product(int amount, int cost, String name){
		this.amount = amount;
		this.cost = cost;
		this.name = name;
	}

	public Product(Product product) {
		this.amount = product.amount;
		this.cost = product.cost;
		this.name = product.name;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}//end Product