package Classes.Entities;



public class Client {

	private String email;
	private String name;

	public Client(String email, String name) {
		this.email = email;
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}
}