package Classes;



public class ClientManagerAction extends WindowAction {

    public ClientManagerAction(GUI frame) {
        super(frame);
    }

    @Override
    public void performAction() {
        frame.setContentPane(new ClientManagerView(frame));
        frame.revalidate();
        frame.repaint();
    }
}
