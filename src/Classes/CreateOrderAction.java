package Classes;

import Classes.Boundary.OrderForm;
import Classes.Controllers.OrderController;


public class CreateOrderAction extends WindowAction {

    public CreateOrderAction(GUI frame) {
        super(frame);
    }

    @Override
    public void performAction() {
        OrderController controller = new OrderController(frame.getDataAccessor());
        OrderForm form = new OrderForm(controller);

        frame.setContentPane(form);
        frame.revalidate();
        frame.repaint();

        controller.startAction();
    }
}
