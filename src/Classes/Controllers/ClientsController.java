package Classes.Controllers;

import Classes.Boundary.DataAccessor;
import Classes.Boundary.ClientsChooser;
import Classes.Entities.Client;
import Classes.Entities.ClientsList;

import java.util.ArrayList;


public class ClientsController {

	private DataAccessor dataAccessor;
	private ClientsChooser clientsChooser;
	private ClientsList clients;

	public ClientsController(DataAccessor dataAccessor) {
		this.dataAccessor = dataAccessor;
	}

	public void finalize() throws Throwable {

	}

	public void sendCatalog(ArrayList<Integer> indices) {
		for (int index : indices) {
			Client client = clients.get(index);
			System.out.println("catalog sent to " + client.getName());
		}

		clientsChooser.showSendingResult(true);
	}

	public void startAction() {
		clients = dataAccessor.getClients();
		clientsChooser.showClients(clients);
	}

	public void setClientsChooser(ClientsChooser clientsChooser) {
		this.clientsChooser = clientsChooser;
	}
}