package Classes.Controllers;

import Classes.Boundary.LoginForm;
import Classes.Boundary.DataAccessor;
import Classes.Entities.User;
import Classes.WindowSession;
import util.Helper;

/**
 * @author ������
 * @version 1.0
 */
public class LoginController extends Controller{

	private LoginForm loginForm;
	private DataAccessor dataAccessor;
	private WindowSession session;

	public LoginController(DataAccessor dataAccessor, WindowSession session){
		this.dataAccessor = dataAccessor;
		this.session = session;
	}

	public void startAction() {
		loginForm.setVisible(true);
	}

	public void doLogin(String name, String password){
		User user = new User(name, password);
		dataAccessor.authenticate(user);

		if (Helper.isRoleValid(user.getRole())) {
			session.put("login", user.getName());
			session.put("role", user.getRole());
		}

		loginForm.showPage(user.getRole());
	}


	public void setLoginForm(LoginForm loginForm) {
		this.loginForm = loginForm;
	}
}