package Classes.Controllers;


import Classes.Boundary.DataAccessor;

public class Controller {
    DataAccessor dataAccessor;

    public DataAccessor getDataAccessor() {
        return dataAccessor;
    }

    public void setDataAccessor(DataAccessor dataAccessor) {
        this.dataAccessor = dataAccessor;
    }
}
