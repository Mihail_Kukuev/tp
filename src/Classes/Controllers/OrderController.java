package Classes.Controllers;

import Classes.Boundary.OrderForm;
import Classes.Boundary.DataAccessor;
import Classes.Entities.Order;
import Classes.Entities.Product;
import Classes.Entities.ProductsList;

import java.util.ArrayList;
import java.util.Map;

/**
 * @author ������
 * @version 1.0
 */
public class OrderController {

	private OrderForm orderForm;
	private DataAccessor dataAccessor;
    private ProductsList productsList;
    private ProductsList selectedProductsList;

	public OrderController(DataAccessor dataAccessor){
        this.dataAccessor = dataAccessor;
	}

    public void startAction() {
        productsList = dataAccessor.getProducts();
        orderForm.showOrderForm(productsList);
    }

    public void selectProducts(ArrayList<Integer> selectedIndices) {
        selectedProductsList = new ProductsList();
        for (int index : selectedIndices) {
            Product selected = new Product(productsList.get(index));
            selected.setAmount(0);
            selectedProductsList.add(selected);
        }

        orderForm.showCostFields(selectedProductsList);
    }

    public void submitOrder(ArrayList<Integer> counts) {
        int totalPrice = 0;
        for (int i = 0; i < counts.size(); ++i) {
            Product selected = selectedProductsList.get(i);
            System.out.println(selected.getName());
            System.out.println(counts.get(i));
            System.out.println(selected.getCost());
            selected.setAmount(counts.get(i));
            totalPrice += selected.getCost() * selected.getAmount();
            System.out.println(selected.getCost() * selected.getAmount());
        }

        orderForm.showCost(totalPrice);
    }

	public void saveOrder(Map<String, String> formData){
        Order order = new Order(formData, selectedProductsList);
        boolean isSuccess = dataAccessor.addOrder(order);
        orderForm.showResult(isSuccess);
	}


    public void setOrderForm(OrderForm orderForm) {
        this.orderForm = orderForm;
    }
}//end OrderController