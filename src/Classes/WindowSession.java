package Classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class WindowSession extends HashMap<String, String> {

    private static final ArrayList<String> PERMITTED_FIELDS =
            new ArrayList<>(Arrays.asList("login", "role"));

    @Override
    public String get(Object key) {
        if (!(key instanceof String))
            return null;

        String keyString = (String)key;
        if (PERMITTED_FIELDS.contains(keyString))
            return super.get(key);

        return null;
    }
}
