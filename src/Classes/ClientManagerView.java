package Classes;

import util.Helper;

import javax.swing.*;


public class ClientManagerView extends JPanel {

    private GUI frame;

    public ClientManagerView(GUI frame) {
        this.frame = frame;

        initComponents();
        setupPanel();
    }

    private void initComponents() {
        add(new JLabel("Hello " + frame.getSession().get("login")));

        JButton createOrderButton = new JButton("Create Order");
        add(createOrderButton);
        createOrderButton.addActionListener(e -> showPage("CreateOrder"));

        JButton sendCatalogButton = new JButton("Send catalog");
        add(sendCatalogButton);
        sendCatalogButton.addActionListener(e -> showPage("SendCatalog"));

        JButton logoutButton = new JButton("Log out");
        add(logoutButton);
        logoutButton.addActionListener(e -> {
            frame.getSession().clear();
            showPage("Login");
        });
    }

    private void showPage(String actionName) {
        WindowAction actionToShow;
        try {
            actionToShow =
                    Helper.actionNameToActionClass(actionName).getConstructor(GUI.class).newInstance(frame);
        } catch (Exception e) {
            actionToShow = new NilAction(frame);
            e.printStackTrace();
        }

        System.out.println(actionToShow.getClass().getName());
        final WindowAction stub = actionToShow;
        new Thread(() -> frame.performAction(stub)).start();
    }

    private void setupPanel() {
        setSize(800, 600);
        setVisible(true);
    }
}
