package Classes;

import javax.swing.*;

/**
 * Created by oliside on 17.5.16.
 */
public class NilAction extends WindowAction {

    public NilAction(GUI frame) {
        super(frame);
    }

    @Override
    public void performAction() {
        JOptionPane.showMessageDialog(
                frame, "Error", "Error", JOptionPane.ERROR_MESSAGE);
    }
}
