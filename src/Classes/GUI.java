package Classes;

import Classes.Boundary.DataAccessor;

import javax.swing.*;
import java.awt.*;


public class GUI extends JFrame {

    private WindowSession session;
    private DataAccessor dataAccessor;

    private GUI() {
        initComponents();
        setupWindow();

        performAction(new LoginAction(this));
    }

    private void initComponents() {
        session = new WindowSession();
        dataAccessor = new DataAccessor();
    }

    private void setupWindow() {
        setLayout(new BorderLayout());
        setSize(800, 600);
        setResizable(false);
        setDefaultLookAndFeelDecorated(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void performAction(WindowAction action) {
        action.performAction();
    }

    public WindowSession getSession() {
        return session;
    }

    public DataAccessor getDataAccessor() {
        return dataAccessor;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(GUI::new);
    }
}
