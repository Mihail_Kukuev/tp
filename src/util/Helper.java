package util;

import Classes.GUI;
import Classes.WindowAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;


public class Helper {

    private static final String actionPrefix = "Classes.";
    private static final String actionPostfix = "Action";
    private static final ArrayList<String> ROLES =
            new ArrayList<>(Arrays.asList("client manager", "nil"));

    public static boolean isRoleValid(String role) {
        return ROLES.contains(role) && !role.equals("nil");
    }

    public static Class<? extends WindowAction> roleToActionClass(String role) throws ClassNotFoundException{
        Class<?> raw = Class.forName(actionPrefix + toCamelCase(role) + actionPostfix);
        return raw.asSubclass(WindowAction.class);
    }

    public static Class<? extends WindowAction> actionNameToActionClass(String actionName)
            throws ClassNotFoundException {
        Class<?> raw = Class.forName(actionPrefix + actionName + actionPostfix);
        return raw.asSubclass(WindowAction.class);
    }

    public static String toCamelCase(String string) {
        StringBuilder builder = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(string);

        while (tokenizer.hasMoreTokens()) {
            String word = tokenizer.nextToken();
            builder.append(Character.toUpperCase(word.charAt(0)));
            builder.append(word.substring(1).toLowerCase());
        }

        return builder.toString();
    }

    public static void showPage(String query, GUI frame) throws ClassNotFoundException{
        Class<? extends WindowAction> action;
        if (ROLES.contains(query)) {
            action = roleToActionClass(query);
        } else {
            action = actionNameToActionClass(query);
        }
    }
}
